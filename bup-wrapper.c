/*
 * (C) 2017 Andrew Heberle (andrew.heberle@gmail.com)
 * Released under: GPL v3 or later.
 *
 * Wrapper for "bup" to apply "CAP_DAC_READ_SEARCH" so backup process can 
 * read all files.
 *
 * Adapted from ambient capability test code written by:
 *
 * (C) 2015 Christoph Lameter <cl@linux.com>
 * Released under: GPL v3 or later.
 *
 *
 * Compile using:
 *
 *      gcc -o bup-wrapper bup-wrapper.c -lcap-ng
 *
 * This program must have the "CAP_DAC_READ_SEARCH" capability to run properly
 *
 * A command to equip the binary with the right caps is:
 *
 *      setcap cap_dac_read_search+p bup-wrapper
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <cap-ng.h>
#include <sys/prctl.h>
#include <linux/capability.h>

/*
 * Definitions from the kernel header files. These are going to be removed
 * when the /usr/include files have these defined.
 */
#define PR_CAP_AMBIENT 47
#define PR_CAP_AMBIENT_IS_SET 1
#define PR_CAP_AMBIENT_RAISE 2
#define PR_CAP_AMBIENT_LOWER 3
#define PR_CAP_AMBIENT_CLEAR_ALL 4

static void set_ambient_cap(int cap) {
	int rc;

	capng_get_caps_process();
	rc = capng_update(CAPNG_ADD, CAPNG_INHERITABLE, cap);
	if (rc) {
		printf("Cannot add inheritable cap\n");
		exit(2);
	}
	capng_apply(CAPNG_SELECT_CAPS);

	/* Note the two 0s at the end. Kernel checks for these */
	if (prctl(PR_CAP_AMBIENT, PR_CAP_AMBIENT_RAISE, cap, 0, 0)) {
		perror("Cannot set cap");
		exit(1);
	}
}

int main(int argc, char **argv, char **envp) {
	int rc;

	set_ambient_cap(CAP_DAC_READ_SEARCH);

	argv[0] = "bup";

	if (execvpe(argv[0], argv, envp)) {
		perror("Cannot exec");
		exit(127);
	}

	return 0;
}
